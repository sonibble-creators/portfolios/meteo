import 'package:flutter/material.dart';
import 'package:meteo/presentation/pages/home/widgets/home_detail_widget.dart';
import 'package:meteo/presentation/pages/home/widgets/main_home_widget.dart';

/// # Home
/// the main page for the user,
/// will show info from the weather,
/// so the user can easily to consume and understand the situations
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: const [
          // the main content goes here
          MainHomeWidget(),

          // the detail content widget
          HomeDetailWidget(),
        ],
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
