import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/location/location_bloc.dart';
import 'package:meteo/presentation/components/search_item_card.dart';
import 'package:meteo/presentation/themes/behavior.dart';
import 'package:shimmer/shimmer.dart';

/// # HomeSearchWidget
/// search all of the location
class HomeSearchWidget extends StatelessWidget {
  const HomeSearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 28.0,
      ),
      child: Column(
        children: [
          // header
          const SizedBox(height: 16.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 44.0,
                height: 6.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).hintColor.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(12.0),
                ),
              ),
            ],
          ),

          // search form
          const SizedBox(height: 24.0),
          TextFormField(
            onFieldSubmitted: (value) {
              // before go we need to check the value is empty or not
              // only run when the value is not empty
              if (value.isNotEmpty) {
                // we will search the related location here
                context
                    .read<LocationBloc>()
                    .add(SearchLocationEvent(locationQuery: value));
              }
            },
            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1.2,
                ),
            decoration: InputDecoration(
              hintText: "Search Your Location",
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
              border: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(16.0),
                borderSide: BorderSide(
                  color: Theme.of(context).dividerColor,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(16.0),
                borderSide: BorderSide(color: Theme.of(context).dividerColor),
              ),
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              hintStyle: Theme.of(context).textTheme.bodyText2!.copyWith(
                    color: Theme.of(context).hintColor,
                  ),
            ),
          ),

          // the result of locations searched
          // this can be empty
          BlocBuilder<LocationBloc, LocationState>(
            builder: (_, state) {
              if (state is SearchedLocationState) {
                // show the actual data here
                return Column(
                  children: [
                    // show the list of data
                    const SizedBox(height: 28.0),
                    SizedBox(
                      height: MediaQuery.of(context).size.height - 300.0,
                      child: ListView.builder(
                        itemCount: state.locations.length,
                        itemBuilder: (context, index) {
                          var item = state.locations[index];

                          return SearchItemCard(item: item);
                        },
                      ),
                    ),
                  ],
                );
              } else if (state is SearchingLocationState) {
                // show the loading
                return Column(
                  children: List.generate(
                    4,
                    (index) => Shimmer.fromColors(
                      child: Container(
                        height: 80.0,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 12.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Row(
                          children: [
                            Shimmer.fromColors(
                              child: Container(
                                width: 56.0,
                                height: 56.0,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(16.0),
                                ),
                              ),
                              baseColor: Theme.of(context)
                                  .dividerColor
                                  .withOpacity(0.4),
                              highlightColor: Theme.of(context)
                                  .dividerColor
                                  .withOpacity(0.7),
                            ),
                            const SizedBox(width: 16.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Shimmer.fromColors(
                                  child: Container(
                                    width: 100.0,
                                    height: 24.0,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(16.0),
                                    ),
                                  ),
                                  baseColor: Theme.of(context)
                                      .dividerColor
                                      .withOpacity(0.4),
                                  highlightColor: Theme.of(context)
                                      .dividerColor
                                      .withOpacity(0.7),
                                ),
                                const SizedBox(height: 12.0),
                                Shimmer.fromColors(
                                  child: Container(
                                    width: 180.0,
                                    height: 18.0,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(16.0),
                                    ),
                                  ),
                                  baseColor: Theme.of(context)
                                      .dividerColor
                                      .withOpacity(0.4),
                                  highlightColor: Theme.of(context)
                                      .dividerColor
                                      .withOpacity(0.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      baseColor:
                          Theme.of(context).dividerColor.withOpacity(0.4),
                      highlightColor:
                          Theme.of(context).dividerColor.withOpacity(0.7),
                    ),
                  ).toList(),
                );
              } else {
                // show the empty state
                // allow the user to know that location they looking for is not found
                return ScrollConfiguration(
                  behavior: CleanScrollBehavior(),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        // image
                        const SizedBox(height: 80.0),
                        Image.asset(
                          "assets/illustrations/writing.png",
                          width: 272.0,
                          height: 272.0,
                          fit: BoxFit.contain,
                        ),

                        // content
                        const SizedBox(height: 32.0),
                        Text(
                          "Not Found",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        const SizedBox(height: 24.0),
                        Text(
                          "Opps, your location not found, Please use another keyword and try again",
                          style: Theme.of(context).textTheme.bodyText2,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                );
              }
            },
          ),
        ],
      ),
    );
  }
}
