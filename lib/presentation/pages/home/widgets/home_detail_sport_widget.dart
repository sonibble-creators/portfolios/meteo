import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/components/detail_sport_item_card.dart';
import 'package:shimmer/shimmer.dart';

/// # HomeDetailSport
/// show sport info for the weather conditions
class HomeDetailSportWidget extends StatelessWidget {
  const HomeDetailSportWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (context, state) {
        // the successful state
        // show the actual data for user
        if (state is SuccessWeatherState) {
          // we need to check the size of content
          var contents = [];
          // add the actual content
          contents.addAll(state.sport.football);
          contents.addAll(state.sport.cricket);
          contents.addAll(state.sport.golf);
          if (contents.isNotEmpty) {
            return ListView.builder(
              padding: const EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 0.0),
              shrinkWrap: true,
              itemCount: contents.length,
              itemBuilder: (context, index) {
                var item = contents[index];

                return DetailSportItemCard(item: item);
              },
            );
          } else {
            // empty state of sports
            return SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 28.0),
              child: Column(
                children: [
                  const SizedBox(height: 32.0),
                  Center(
                    child: Image.asset(
                      "assets/illustrations/writing.png",
                      width: 272.0,
                      height: 272.0,
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Text(
                    "Not Found",
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.white.withOpacity(0.8),
                        ),
                  ),
                  const SizedBox(height: 24.0),
                  Text(
                    "Opps, today no event found, please see the update later. Enjoy your moment without worrying about weather",
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: Colors.white.withOpacity(0.8),
                        ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            );
          }
        } else {
          return SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 28.0),
            child: Column(
              children: [
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
