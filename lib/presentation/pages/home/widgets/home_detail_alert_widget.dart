import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/components/detail_alert_item_card.dart';
import 'package:shimmer/shimmer.dart';

/// # HomeDetailAlertWidget
/// show detail info about the alert for specify weather condition
class HomeDetailAlertWidget extends StatelessWidget {
  const HomeDetailAlertWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (context, state) {
        if (state is SuccessWeatherState) {
          // we need to check the content lengt
          if (state.forecast.alerts.alert.isNotEmpty) {
            return ListView.builder(
              padding: const EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 0.0),
              shrinkWrap: true,
              itemCount: state.forecast.alerts.alert.length,
              itemBuilder: (context, index) {
                var item = state.forecast.alerts.alert[index];

                return DetailAlertItemCard(item: item);
              },
            );
          } else {
            return SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 28.0),
              child: Column(
                children: [
                  const SizedBox(height: 32.0),
                  Center(
                    child: Image.asset(
                      "assets/illustrations/writing.png",
                      width: 272.0,
                      height: 272.0,
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Text(
                    "Not Found",
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.white.withOpacity(0.8),
                        ),
                  ),
                  const SizedBox(height: 24.0),
                  Text(
                    "Opps, today no alerts found, please see the update later. Enjoy your moment without worrying about weather",
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: Colors.white.withOpacity(0.8),
                        ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            );
          }
        }
        return SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: Column(
            children: [
              const SizedBox(height: 16.0),
              Shimmer.fromColors(
                child: Container(
                  height: 100.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                baseColor: Colors.white.withOpacity(0.04),
                highlightColor: Colors.white.withOpacity(0.06),
              ),
              const SizedBox(height: 16.0),
              Shimmer.fromColors(
                child: Container(
                  height: 100.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                baseColor: Colors.white.withOpacity(0.04),
                highlightColor: Colors.white.withOpacity(0.06),
              ),
              const SizedBox(height: 16.0),
              Shimmer.fromColors(
                child: Container(
                  height: 100.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                baseColor: Colors.white.withOpacity(0.04),
                highlightColor: Colors.white.withOpacity(0.06),
              ),
              const SizedBox(height: 16.0),
              Shimmer.fromColors(
                child: Container(
                  height: 100.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                baseColor: Colors.white.withOpacity(0.04),
                highlightColor: Colors.white.withOpacity(0.06),
              ),
            ],
          ),
        );
      },
    );
  }
}
