import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:shimmer/shimmer.dart';

/// # HomeDetailAstronoy
/// show the astronomy info, like sunrise, sunset, monrise, etc.
class HomeDetailAstronomyWidget extends StatelessWidget {
  const HomeDetailAstronomyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 28.0),
      child: BlocBuilder<WeatherBloc, WeatherState>(
        builder: (context, state) {
          // the state success
          // show the data into user
          if (state is SuccessWeatherState) {
            return Column(
              children: [
                const SizedBox(height: 28.0),
                Container(
                  height: 98.0,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 12.0),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.04),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/sunrise-light.png",
                        width: 78.0,
                        height: 78.0,
                      ),
                      const SizedBox(width: 16.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Sunrise",
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: Colors.white.withOpacity(0.8),
                                    ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            state.forecast.forecast.forecastday.first.astro
                                .sunrise,
                            style:
                                Theme.of(context).textTheme.bodyText2!.copyWith(
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                // sunset section
                const SizedBox(height: 16.0),
                Container(
                  height: 98.0,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 12.0),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.04),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/sunset-light.png",
                        width: 78.0,
                        height: 78.0,
                      ),
                      const SizedBox(width: 16.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Sunset",
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: Colors.white.withOpacity(0.8),
                                    ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            state.forecast.forecast.forecastday.first.astro
                                .sunset,
                            style:
                                Theme.of(context).textTheme.bodyText2!.copyWith(
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                // moonrise section
                const SizedBox(height: 16.0),
                Container(
                  height: 98.0,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 12.0),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.04),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/full-moon-light.png",
                        width: 78.0,
                        height: 78.0,
                      ),
                      const SizedBox(width: 16.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Moonrise",
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: Colors.white.withOpacity(0.8),
                                    ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            state.forecast.forecast.forecastday.first.astro
                                .moonrise,
                            style:
                                Theme.of(context).textTheme.bodyText2!.copyWith(
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                // moonset section
                const SizedBox(height: 16.0),
                Container(
                  height: 98.0,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 12.0),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.04),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/half-moon-light.png",
                        width: 78.0,
                        height: 78.0,
                      ),
                      const SizedBox(width: 16.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Moonset",
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: Colors.white.withOpacity(0.8),
                                    ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            state.forecast.forecast.forecastday.first.astro
                                .moonset,
                            style:
                                Theme.of(context).textTheme.bodyText2!.copyWith(
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return Column(
              children: [
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
                const SizedBox(height: 16.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 100.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.06),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
