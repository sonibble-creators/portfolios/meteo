import 'package:flutter/material.dart';
import 'package:meteo/presentation/routes/routes.dart';

/// # Splash Page
/// showing splash and loading for the application
/// when the application run for the first time
class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  // after seeing the splash screen,
  // user will be bring into the ondboarding,
  // however, when the user already see the obboarding for the first time
  // they will bring into home page
  void next(context) {
    Future.delayed(
      const Duration(milliseconds: 1800),
      () {
        // will replce the splash screen with the navigator
        Navigator.pushReplacementNamed(context, Routes.onboarding);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // we need to call the next function
    next(context);

    return Scaffold(
      body: Center(
        child: Container(
          height: 92.0,
          width: 92.0,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).primaryColor.withOpacity(0.3),
                blurRadius: 60.0,
                spreadRadius: -2,
                offset: const Offset(0, 4),
              ),
            ],
            borderRadius: BorderRadius.circular(32.0),
            image: const DecorationImage(
              image: AssetImage(
                "assets/images/logo.png",
              ),
            ),
          ),
        ),
      ),
    );
  }
}
