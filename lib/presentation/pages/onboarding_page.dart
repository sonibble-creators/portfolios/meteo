import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/account/account_bloc.dart';
import 'package:meteo/presentation/routes/routes.dart';
import 'package:meteo/presentation/themes/behavior.dart';

/// # Boarding
/// on boarding the user when the first time come, and
/// let the user know about our best feature in app
class OnBoardingPage extends StatelessWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ScrollConfiguration(
          behavior: CleanScrollBehavior(),
          child: SingleChildScrollView(
            padding:
                const EdgeInsets.symmetric(horizontal: 28.0, vertical: 20.0),
            child: BlocConsumer<AccountBloc, AccountState>(
              builder: (context, state) {
                return Column(
                  children: [
                    const SizedBox(height: 92.0),

                    // image about content
                    Center(
                      child: Image.asset(
                        "assets/icons/mostly-cloud-light.png",
                        height: 240.0,
                        width: 240.0,
                      ),
                    ),

                    // content start here
                    const SizedBox(height: 84.0),
                    Text(
                      "Weather Info &  News",
                      style: Theme.of(context).textTheme.headline1,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 24.0),
                    Text(
                      "Explore the weather and get know about the nature. Make sure you bring umbrella and stay safe",
                      style: Theme.of(context).textTheme.bodyText1,
                      textAlign: TextAlign.center,
                    ),

                    // we need to add the button
                    const SizedBox(height: 64.0),
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: ElevatedButton(
                        onPressed: () {
                          // go to home page
                          // will replace the onboarding with the home
                          context
                              .read<AccountBloc>()
                              .add(OnBoardedAccountEvent());

                          Navigator.pushReplacementNamed(context, Routes.home);
                        },
                        child: const Text("Explore Now"),
                      ),
                    ),
                    const SizedBox(height: 20.0),
                  ],
                );
              },
              listener: (context, state) {
                if (state is SuccessLoadedAccountState) {
                  // we need to check the onboarding process
                  if (state.isOnBoarded) {
                    // we are going to push to home
                    Navigator.pushReplacementNamed(context, Routes.home);
                  }
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
