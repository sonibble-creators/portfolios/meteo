String weatherIconDecoder(int code, int day) {
  String icon = "assets/icons/full-moon-light.png";

  // we need to filter all icons with their code of condition
  var selectedIcon =
      weatherIcons.asMap().values.where((element) => element["code"] == code);

  // we need to check the icons was found
  // and split the icon by the day condition
  if (selectedIcon.isNotEmpty) {
    if (day == 1) {
      icon = selectedIcon.first["dayIcon"] as String;
    } else {
      icon = selectedIcon.first["nightIcon"] as String;
    }
  }

  return icon;
}

var weatherIcons = [
  {
    "code": 1000,
    "dayIcon": "assets/icons/sun-light.png",
    "nightIcon": "assets/icons/full-moon-light.png",
  },
  {
    "code": 1003,
    "dayIcon": "assets/icons/mostly-cloud-light.png",
    "nightIcon": "assets/icons/cloudy-night-lighter.png",
  },
  {
    "code": 1006,
    "dayIcon": "assets/icons/mostly-cloud-light.png",
    "nightIcon": "assets/icons/mostly-cloudy-light.png",
  },
  {
    "code": 1009,
    "dayIcon": "assets/icons/mostly-cloud-light.png",
    "nightIcon": "assets/icons/cloudy-night-light.png",
  },
  {
    "code": 1030,
    "dayIcon": "assets/icons/heavy-wind-light.png",
    "nightIcon": "assets/icons/heavy-wind-light.png",
  },
  {
    "code": 1063,
    "dayIcon": "assets/icons/rainyday-light.png",
    "nightIcon": "assets/icons/hailstrom-light.png",
  },
  {
    "code": 1066,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1069,
    "dayIcon": "assets/icons/hailstrom-light.png",
    "nightIcon": "assets/icons/hailstrom-light.png",
  },
  {
    "code": 1072,
    "dayIcon": "assets/icons/hailstrom-light.png",
    "nightIcon": "assets/icons/hailstrom-light.png",
  },
  {
    "code": 1087,
    "dayIcon": "assets/icons/thunderstorm-light.png",
    "nightIcon": "assets/icons/thunderstorm-light.png",
  },
  {
    "code": 1114,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1117,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1135,
    "dayIcon": "assets/icons/cloud-light.png",
    "nightIcon": "assets/icons/cloud-light.png",
  },
  {
    "code": 1147,
    "dayIcon": "assets/icons/mostly-cloudy-light.png",
    "nightIcon": "assets/icons/mostly-cloudy-light.png",
  },
  {
    "code": 1150,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1153,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1168,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1171,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1180,
    "dayIcon": "assets/icons/rain-light.png",
    "nightIcon": "assets/icons/rain-light.png",
  },
  {
    "code": 1183,
    "dayIcon": "assets/icons/rain-light.png",
    "nightIcon": "assets/icons/rain-light.png",
  },
  {
    "code": 1186,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1189,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1192,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1195,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1198,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1201,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1204,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1207,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1210,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1213,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1216,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1219,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1222,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1225,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1225,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1237,
    "dayIcon": "assets/icons/hailstrom-light.png",
    "nightIcon": "assets/icons/hailstrom-light.png",
  },
  {
    "code": 1240,
    "dayIcon": "assets/icons/rain-light.png",
    "nightIcon": "assets/icons/rain-light.png",
  },
  {
    "code": 1243,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1246,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1246,
    "dayIcon": "assets/icons/heavy-rain-light.png",
    "nightIcon": "assets/icons/heavy-rain-light.png",
  },
  {
    "code": 1249,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1252,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1255,
    "dayIcon": "assets/icons/snow-light.png",
    "nightIcon": "assets/icons/snow-light.png",
  },
  {
    "code": 1258,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1261,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1264,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
  {
    "code": 1273,
    "dayIcon": "assets/icons/thunderstorm-light.png",
    "nightIcon": "assets/icons/thunderstorm-light.png",
  },
  {
    "code": 1276,
    "dayIcon": "assets/icons/thunderstorm-light.png",
    "nightIcon": "assets/icons/thunderstorm-light.png",
  },
  {
    "code": 1279,
    "dayIcon": "assets/icons/thunderstorm-light.png",
    "nightIcon": "assets/icons/thunderstorm-light.png",
  },
  {
    "code": 1282,
    "dayIcon": "assets/icons/heavy-snowfall-light.png",
    "nightIcon": "assets/icons/heavy-snowfall-light.png",
  },
];
