import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:meteo/data/payloads/weather_payload.dart';

/// # DetailAlertItemCard
/// detail item of alert
class DetailAlertItemCard extends StatelessWidget {
  final WeatherAlertDetailPayload item;

  const DetailAlertItemCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.04),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            IconlyBold.notification,
            size: 48.0,
            color: Colors.white.withOpacity(0.8),
          ),
          const SizedBox(width: 16.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.category.isNotEmpty ? item.category : item.headline,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: Colors.white.withOpacity(0.8),
                      ),
                ),
                const SizedBox(height: 8.0),
                Text(
                  item.note.isNotEmpty
                      ? item.note
                      : item.instruction.isNotEmpty
                          ? item.instruction
                          : item.desc,
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        color: Colors.white70,
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
