import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:meteo/data/payloads/weather_payload.dart';
import 'package:meteo/presentation/utils/weather_icon_decoder.dart';

/// # ForecastItemcard
/// item card for single day forecast
class DayForecastItemCard extends StatelessWidget {
  final WeatherForecastDayItemPayload item;

  const DayForecastItemCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Row(
        children: [
          Image.asset(
            weatherIconDecoder(item.day.condition.code, 1),
            width: 76.0,
            height: 76.0,
            fit: BoxFit.contain,
          ),
          const SizedBox(width: 16.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  DateFormat("EEEE").format(DateTime.parse(item.date)),
                  style: Theme.of(context).textTheme.subtitle2!.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                ),
                const SizedBox(height: 12.0),
                Text(
                  DateFormat("MMMM, dd yyyy").format(DateTime.parse(item.date)),
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        color: Theme.of(context).hintColor,
                      ),
                ),
                const SizedBox(height: 4.0),
                Row(
                  children: [
                    Text(
                      item.day.condition.text,
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.w700,
                          ),
                    ),
                    const SizedBox(width: 16.0),
                    Text(
                      "${item.day.avgtemp_c.round()}° C",
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.w700,
                          ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
