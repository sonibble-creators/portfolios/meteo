import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:intl/intl.dart';
import 'package:meteo/data/payloads/weather_payload.dart';

/// # DetailSportItemCard
/// item card of detail weather
class DetailSportItemCard extends StatelessWidget {
  final WeatherSportItemPayload item;
  const DetailSportItemCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.04),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Row(
        children: [
          Icon(
            IconlyBold.game,
            size: 48.0,
            color: Colors.white.withOpacity(0.8),
          ),
          const SizedBox(width: 16.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.tournament,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: Colors.white.withOpacity(0.8),
                      ),
                ),
                const SizedBox(height: 8.0),
                Text(
                  item.stadium,
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        color: Colors.white70,
                      ),
                ),
                Text(
                  DateFormat("EEEE, MMMM dd yyyy")
                      .format(DateTime.parse(item.start)),
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        color: Colors.white.withOpacity(0.8),
                        fontWeight: FontWeight.w600,
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
