import 'package:flutter/material.dart';
import 'package:meteo/data/payloads/weather_payload.dart';
import 'package:meteo/presentation/utils/weather_icon_decoder.dart';

/// # DayForecastItemcard
/// the item of day forecast,
/// contain summary data about the weather
class HourForecastItemCard extends StatelessWidget {
  /// margin
  /// allow to adding an margin in each of item
  /// usually this will used when get inside an listview
  /// to give more space
  final EdgeInsets margin;

  /// item
  /// the item for hourly forecast
  final WeatherHourForecastItemPayload item;

  const HourForecastItemCard({
    Key? key,
    this.margin = EdgeInsets.zero,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      width: 100.0,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.08),
        border: Border.all(color: Colors.white.withOpacity(0.08)),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        children: [
          // icon weather
          Image.asset(
            weatherIconDecoder(item.condition.code, item.is_day),
            width: 76.0,
            height: 76.0,
            fit: BoxFit.contain,
          ),

          const SizedBox(height: 12.0),
          Text(
            TimeOfDay.fromDateTime(DateTime.parse(item.time))
                .format(context)
                .toString(),
            style: Theme.of(context).textTheme.caption!.copyWith(
                  color: Colors.white70,
                ),
          ),
          Text(
            "${item.feelslike_c.round()}° C",
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  color: Colors.white.withOpacity(0.8),
                ),
          ),
        ],
      ),
    );
  }
}
