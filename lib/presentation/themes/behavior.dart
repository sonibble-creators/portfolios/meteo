import 'package:flutter/material.dart';

/// # CleanScrollBehavior
/// use to remove the behavior from the scroll view
/// this will improve the ui design profesionality
class CleanScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
