import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/data/repositories/account_repository.dart';

part 'account_event.dart';
part 'account_state.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  final AccountRepository _accountRepository = AccountRepository();

  AccountBloc() : super(InitialAccountState()) {
    // onboarded event
    on<OnBoardedAccountEvent>((event, emit) async {
      // we will set the user as onboarded
      try {
        await _accountRepository.userOnboarded();
      } catch (e) {
        // something error
      }
    });

    // load account event
    on<LoadAccountEvent>((event, emit) async {
      // first we need to show load
      emit(LoadingAccountState());

      try {
        // check the onboarding
        var isOnboarded = await _accountRepository.isOnboarded();

        emit(SuccessLoadedAccountState(isOnBoarded: isOnboarded));
      } catch (e) {
        // something wrong
        emit(FailureAccountState());
      }
    });
  }
}
