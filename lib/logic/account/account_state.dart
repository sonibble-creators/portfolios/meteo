part of 'account_bloc.dart';

@immutable
abstract class AccountState {}

class InitialAccountState extends AccountState {}

class LoadingAccountState extends AccountState {}

class OnBoardedAccountState extends AccountState {}

class SuccessLoadedAccountState extends AccountState {
  final bool isOnBoarded;

  SuccessLoadedAccountState({
    required this.isOnBoarded,
  });
}

class FailureAccountState extends AccountState {}
