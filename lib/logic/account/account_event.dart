part of 'account_bloc.dart';

@immutable
abstract class AccountEvent {}

class OnBoardedAccountEvent extends AccountEvent {}

class LoadAccountEvent extends AccountEvent {}
