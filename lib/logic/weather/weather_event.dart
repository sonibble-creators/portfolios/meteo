part of 'weather_bloc.dart';

@immutable
abstract class WeatherEvent {}

class LoadWeatherEvent extends WeatherEvent {}

class LoadWeatherByLocationPickedEvent extends WeatherEvent {
  final double lat;
  final double lon;

  LoadWeatherByLocationPickedEvent({
    required this.lat,
    required this.lon,
  });
}
