import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/data/payloads/weather_payload.dart';
import 'package:meteo/data/repositories/location_repository.dart';
import 'package:meteo/data/repositories/weather_repository.dart';

part 'weather_state.dart';
part 'weather_event.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository _weatherRepository = WeatherRepository();
  final LocationRepository _locationRepository = LocationRepository();

  WeatherBloc() : super(InitialWeatherState()) {
    on<LoadWeatherByLocationPickedEvent>((event, emit) async {
      emit(LoadingWeatherState());

      try {
        // load the forecast data of weather
        var forecast = await _weatherRepository.loadForecast(
            queryLocation: "${event.lat},${event.lon}");

        // load the sports
        var sports = await _weatherRepository.loadSports(
            queryLocation: "${event.lat},${event.lon}");

        emit(SuccessWeatherState(forecast: forecast!, sport: sports!));
      } catch (e) {
        // opps, something wrong
        emit(NotFoundWeatherState());
      }
    });

    on<LoadWeatherEvent>((event, emit) async {
      emit(LoadingWeatherState());

      try {
        // we need to load the current location
        var location = await _locationRepository.loadCurrentLocation();

        // load the forecast data of weather
        var forecast = await _weatherRepository.loadForecast(
            queryLocation: "${location.latitude},${location.longitude}");

        // load the sports
        var sports = await _weatherRepository.loadSports(
            queryLocation: "${location.latitude},${location.longitude}");

        emit(SuccessWeatherState(forecast: forecast!, sport: sports!));
      } catch (e) {
        // opps, something wrong
        emit(NotFoundWeatherState());
      }
    });
  }
}
