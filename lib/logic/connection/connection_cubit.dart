import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'connection_state.dart';

/// # ConnectionCubit
class ConnectionCubit extends Cubit<ConnectivityState> {
  ConnectionCubit() : super(InitialConnectivityState());

  /// ## checkConnection
  /// check the connection and update some ui
  /// to show the connection process
  Future<void> checkConnection() async {
    // we will check the connectivity once,
    // and show the reult
    var connection = await (Connectivity().checkConnectivity());

    // we will check the connection type, maybe wifi, mobile, or even not
    // connected
    if (connection == ConnectivityResult.mobile) {
      // alright, connected into mobile network
      emit(ConnectedConnectivityState(connectionType: ConnectionType.mobile));
    } else if (connection == ConnectivityResult.wifi) {
      // connected into wifi network
      emit(ConnectedConnectivityState(connectionType: ConnectionType.wifi));
    } else {
      // hmm, no network connected, we going to update the state
      emit(DisconnectedConnectivityState());
    }

    // ok, we need to listen into connection,
    // just make sure the connection connect,
    // and notify when connection is change
    Connectivity().onConnectivityChanged.listen((connection) {
      // we will check the connection type, maybe wifi, mobile, or even not
      // connected
      if (connection == ConnectivityResult.mobile) {
        // alright, connected into mobile network
        emit(ConnectedConnectivityState(connectionType: ConnectionType.mobile));
      } else if (connection == ConnectivityResult.wifi) {
        // connected into wifi network
        emit(ConnectedConnectivityState(connectionType: ConnectionType.wifi));
      } else {
        // hmm, no network connected, we going to update the state
        emit(DisconnectedConnectivityState());
      }
    });
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
