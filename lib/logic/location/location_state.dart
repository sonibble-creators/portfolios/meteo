part of 'location_bloc.dart';

@immutable
abstract class LocationState {}

class LocationInitialState extends LocationState {}

class SearchingLocationState extends LocationState {
  SearchingLocationState();
}

class SearchedLocationState extends LocationState {
  final List<WeatherLocationPayload> locations;

  SearchedLocationState({
    required this.locations,
  });
}

class EmptySearchLocationState extends LocationState {
  EmptySearchLocationState();
}
