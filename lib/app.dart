import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/account/account_bloc.dart';
import 'package:meteo/logic/connection/connection_cubit.dart';
import 'package:meteo/logic/location/location_bloc.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/components/no_connection.dart';
import 'package:meteo/presentation/routes/routes.dart';
import 'package:meteo/presentation/themes/theme.dart';

/// # Meteo App
/// The main app, configuration, for app settings, routes, theme
class MeteoApp extends StatelessWidget {
  const MeteoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => ConnectionCubit()..checkConnection(),
          lazy: false,
        ),
        BlocProvider(
          create: (_) => AccountBloc()..add(LoadAccountEvent()),
        ),
        BlocProvider(
          create: (_) => WeatherBloc()..add(LoadWeatherEvent()),
          lazy: false,
        ),
        BlocProvider(
          create: (_) => LocationBloc(),
        ),
      ],
      child: BlocListener<ConnectionCubit, ConnectivityState>(
        listener: (_, state) {
          // we will show some alerts, if the connection was not found
          if (state is DisconnectedConnectivityState) {
            showDialog(
              context: context,
              builder: (context) {
                return const NoConnectionWidget();
              },
            );
          }
        },
        child: MaterialApp(
          title: "Meteo",
          debugShowCheckedModeBanner: false,
          themeMode: ThemeMode.system,
          theme: MeteoTheme.lightTheme(),
          darkTheme: MeteoTheme.darkTheme(),
          initialRoute: Routes.initial,
          routes: Routes.allRoutes,
        ),
      ),
    );
  }
}
