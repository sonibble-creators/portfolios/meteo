// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:flutter/foundation.dart';

/// # CurrentDetailPayload
/// detail payload for current condition
/// weather
class CurrentDetailPayload {
  int last_updated_epoch;
  String last_updated;
  double temp_c;
  double temp_f;
  int is_day;
  WeatherConditionPayload condition;
  double wind_mph;
  double wind_kph;
  int wind_degree;
  String wind_dir;
  double pressure_mb;
  double pressure_in;
  double precip_mm;
  double precip_in;
  int humidity;
  int cloud;
  double feelslike_c;
  double feelslike_f;
  double vis_km;
  double vis_miles;
  double uv;
  double gust_mph;
  double gust_kph;
  WeatherAirQualityPayload air_quality;

  CurrentDetailPayload({
    required this.last_updated_epoch,
    required this.last_updated,
    required this.temp_c,
    required this.temp_f,
    required this.is_day,
    required this.condition,
    required this.wind_mph,
    required this.wind_kph,
    required this.wind_degree,
    required this.wind_dir,
    required this.pressure_mb,
    required this.pressure_in,
    required this.precip_mm,
    required this.precip_in,
    required this.humidity,
    required this.cloud,
    required this.feelslike_c,
    required this.feelslike_f,
    required this.vis_km,
    required this.vis_miles,
    required this.uv,
    required this.gust_mph,
    required this.gust_kph,
    required this.air_quality,
  });

  CurrentDetailPayload copyWith({
    int? last_updated_epoch,
    String? last_updated,
    double? temp_c,
    double? temp_f,
    int? is_day,
    WeatherConditionPayload? condition,
    double? wind_mph,
    double? wind_kph,
    int? wind_degree,
    String? wind_dir,
    double? pressure_mb,
    double? pressure_in,
    double? precip_mm,
    double? precip_in,
    int? humidity,
    int? cloud,
    double? feelslike_c,
    double? feelslike_f,
    double? vis_km,
    double? vis_miles,
    double? uv,
    double? gust_mph,
    double? gust_kph,
    WeatherAirQualityPayload? air_quality,
  }) {
    return CurrentDetailPayload(
      last_updated_epoch: last_updated_epoch ?? this.last_updated_epoch,
      last_updated: last_updated ?? this.last_updated,
      temp_c: temp_c ?? this.temp_c,
      temp_f: temp_f ?? this.temp_f,
      is_day: is_day ?? this.is_day,
      condition: condition ?? this.condition,
      wind_mph: wind_mph ?? this.wind_mph,
      wind_kph: wind_kph ?? this.wind_kph,
      wind_degree: wind_degree ?? this.wind_degree,
      wind_dir: wind_dir ?? this.wind_dir,
      pressure_mb: pressure_mb ?? this.pressure_mb,
      pressure_in: pressure_in ?? this.pressure_in,
      precip_mm: precip_mm ?? this.precip_mm,
      precip_in: precip_in ?? this.precip_in,
      humidity: humidity ?? this.humidity,
      cloud: cloud ?? this.cloud,
      feelslike_c: feelslike_c ?? this.feelslike_c,
      feelslike_f: feelslike_f ?? this.feelslike_f,
      vis_km: vis_km ?? this.vis_km,
      vis_miles: vis_miles ?? this.vis_miles,
      uv: uv ?? this.uv,
      gust_mph: gust_mph ?? this.gust_mph,
      gust_kph: gust_kph ?? this.gust_kph,
      air_quality: air_quality ?? this.air_quality,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'last_updated_epoch': last_updated_epoch,
      'last_updated': last_updated,
      'temp_c': temp_c,
      'temp_f': temp_f,
      'is_day': is_day,
      'condition': condition.toMap(),
      'wind_mph': wind_mph,
      'wind_kph': wind_kph,
      'wind_degree': wind_degree,
      'wind_dir': wind_dir,
      'pressure_mb': pressure_mb,
      'pressure_in': pressure_in,
      'precip_mm': precip_mm,
      'precip_in': precip_in,
      'humidity': humidity,
      'cloud': cloud,
      'feelslike_c': feelslike_c,
      'feelslike_f': feelslike_f,
      'vis_km': vis_km,
      'vis_miles': vis_miles,
      'uv': uv,
      'gust_mph': gust_mph,
      'gust_kph': gust_kph,
      'air_quality': air_quality.toMap(),
    };
  }

  factory CurrentDetailPayload.fromMap(Map<String, dynamic> map) {
    return CurrentDetailPayload(
      last_updated_epoch: map['last_updated_epoch']?.toInt() ?? 0,
      last_updated: map['last_updated'] ?? '',
      temp_c: map['temp_c']?.toDouble() ?? 0.0,
      temp_f: map['temp_f']?.toDouble() ?? 0.0,
      is_day: map['is_day']?.toInt() ?? 0,
      condition: WeatherConditionPayload.fromMap(map['condition']),
      wind_mph: map['wind_mph']?.toDouble() ?? 0.0,
      wind_kph: map['wind_kph']?.toDouble() ?? 0.0,
      wind_degree: map['wind_degree']?.toInt() ?? 0,
      wind_dir: map['wind_dir'] ?? '',
      pressure_mb: map['pressure_mb']?.toDouble() ?? 0.0,
      pressure_in: map['pressure_in']?.toDouble() ?? 0.0,
      precip_mm: map['precip_mm']?.toDouble() ?? 0.0,
      precip_in: map['precip_in']?.toDouble() ?? 0.0,
      humidity: map['humidity']?.toInt() ?? 0,
      cloud: map['cloud']?.toInt() ?? 0,
      feelslike_c: map['feelslike_c']?.toDouble() ?? 0.0,
      feelslike_f: map['feelslike_f']?.toDouble() ?? 0.0,
      vis_km: map['vis_km']?.toDouble() ?? 0.0,
      vis_miles: map['vis_miles']?.toDouble() ?? 0.0,
      uv: map['uv']?.toDouble() ?? 0.0,
      gust_mph: map['gust_mph']?.toDouble() ?? 0.0,
      gust_kph: map['gust_kph']?.toDouble() ?? 0.0,
      air_quality: WeatherAirQualityPayload.fromMap(map['air_quality']),
    );
  }

  String toJson() => json.encode(toMap());

  factory CurrentDetailPayload.fromJson(String source) =>
      CurrentDetailPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CurrentDetailPayload(last_updated_epoch: $last_updated_epoch, last_updated: $last_updated, temp_c: $temp_c, temp_f: $temp_f, is_day: $is_day, condition: $condition, wind_mph: $wind_mph, wind_kph: $wind_kph, wind_degree: $wind_degree, wind_dir: $wind_dir, pressure_mb: $pressure_mb, pressure_in: $pressure_in, precip_mm: $precip_mm, precip_in: $precip_in, humidity: $humidity, cloud: $cloud, feelslike_c: $feelslike_c, feelslike_f: $feelslike_f, vis_km: $vis_km, vis_miles: $vis_miles, uv: $uv, gust_mph: $gust_mph, gust_kph: $gust_kph, air_quality: $air_quality)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CurrentDetailPayload &&
        other.last_updated_epoch == last_updated_epoch &&
        other.last_updated == last_updated &&
        other.temp_c == temp_c &&
        other.temp_f == temp_f &&
        other.is_day == is_day &&
        other.condition == condition &&
        other.wind_mph == wind_mph &&
        other.wind_kph == wind_kph &&
        other.wind_degree == wind_degree &&
        other.wind_dir == wind_dir &&
        other.pressure_mb == pressure_mb &&
        other.pressure_in == pressure_in &&
        other.precip_mm == precip_mm &&
        other.precip_in == precip_in &&
        other.humidity == humidity &&
        other.cloud == cloud &&
        other.feelslike_c == feelslike_c &&
        other.feelslike_f == feelslike_f &&
        other.vis_km == vis_km &&
        other.vis_miles == vis_miles &&
        other.uv == uv &&
        other.gust_mph == gust_mph &&
        other.gust_kph == gust_kph &&
        other.air_quality == air_quality;
  }

  @override
  int get hashCode {
    return last_updated_epoch.hashCode ^
        last_updated.hashCode ^
        temp_c.hashCode ^
        temp_f.hashCode ^
        is_day.hashCode ^
        condition.hashCode ^
        wind_mph.hashCode ^
        wind_kph.hashCode ^
        wind_degree.hashCode ^
        wind_dir.hashCode ^
        pressure_mb.hashCode ^
        pressure_in.hashCode ^
        precip_mm.hashCode ^
        precip_in.hashCode ^
        humidity.hashCode ^
        cloud.hashCode ^
        feelslike_c.hashCode ^
        feelslike_f.hashCode ^
        vis_km.hashCode ^
        vis_miles.hashCode ^
        uv.hashCode ^
        gust_mph.hashCode ^
        gust_kph.hashCode ^
        air_quality.hashCode;
  }
}

/// # WeatherLocationPayload
/// detail payload for location in weather
class WeatherLocationPayload {
  String location;
  String name;
  String region;
  String country;
  double lat;
  double lon;
  String tz_id;
  int localtime_epoch;

  WeatherLocationPayload({
    required this.location,
    required this.name,
    required this.region,
    required this.country,
    required this.lat,
    required this.lon,
    required this.tz_id,
    required this.localtime_epoch,
  });

  WeatherLocationPayload copyWith({
    String? location,
    String? name,
    String? region,
    String? country,
    double? lat,
    double? lon,
    String? tz_id,
    int? localtime_epoch,
  }) {
    return WeatherLocationPayload(
      location: location ?? this.location,
      name: name ?? this.name,
      region: region ?? this.region,
      country: country ?? this.country,
      lat: lat ?? this.lat,
      lon: lon ?? this.lon,
      tz_id: tz_id ?? this.tz_id,
      localtime_epoch: localtime_epoch ?? this.localtime_epoch,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'location': location,
      'name': name,
      'region': region,
      'country': country,
      'lat': lat,
      'lon': lon,
      'tz_id': tz_id,
      'localtime_epoch': localtime_epoch,
    };
  }

  factory WeatherLocationPayload.fromMap(Map<String, dynamic> map) {
    return WeatherLocationPayload(
      location: map['location'] ?? '',
      name: map['name'] ?? '',
      region: map['region'] ?? '',
      country: map['country'] ?? '',
      lat: map['lat']?.toDouble() ?? 0.0,
      lon: map['lon']?.toDouble() ?? 0.0,
      tz_id: map['tz_id'] ?? '',
      localtime_epoch: map['localtime_epoch']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherLocationPayload.fromJson(String source) =>
      WeatherLocationPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherLocationPayload(location: $location, name: $name, region: $region, country: $country, lat: $lat, lon: $lon, tz_id: $tz_id, localtime_epoch: $localtime_epoch)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherLocationPayload &&
        other.location == location &&
        other.name == name &&
        other.region == region &&
        other.country == country &&
        other.lat == lat &&
        other.lon == lon &&
        other.tz_id == tz_id &&
        other.localtime_epoch == localtime_epoch;
  }

  @override
  int get hashCode {
    return location.hashCode ^
        name.hashCode ^
        region.hashCode ^
        country.hashCode ^
        lat.hashCode ^
        lon.hashCode ^
        tz_id.hashCode ^
        localtime_epoch.hashCode;
  }
}

/// #WeatherAirQualityPayload
/// payload from api to get the air condition
class WeatherAirQualityPayload {
  double co;
  double no2;
  double o3;
  double so2;
  double pm2_5;
  double pm10;

  WeatherAirQualityPayload({
    required this.co,
    required this.no2,
    required this.o3,
    required this.so2,
    required this.pm2_5,
    required this.pm10,
  });

  WeatherAirQualityPayload copyWith({
    double? co,
    double? no2,
    double? o3,
    double? so2,
    double? pm2_5,
    double? pm10,
  }) {
    return WeatherAirQualityPayload(
      co: co ?? this.co,
      no2: no2 ?? this.no2,
      o3: o3 ?? this.o3,
      so2: so2 ?? this.so2,
      pm2_5: pm2_5 ?? this.pm2_5,
      pm10: pm10 ?? this.pm10,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'co': co,
      'no2': no2,
      'o3': o3,
      'so2': so2,
      'pm2_5': pm2_5,
      'pm10': pm10,
    };
  }

  factory WeatherAirQualityPayload.fromMap(Map<String, dynamic> map) {
    return WeatherAirQualityPayload(
      co: map['co']?.toDouble() ?? 0.0,
      no2: map['no2']?.toDouble() ?? 0.0,
      o3: map['o3']?.toDouble() ?? 0.0,
      so2: map['so2']?.toDouble() ?? 0.0,
      pm2_5: map['pm2_5']?.toDouble() ?? 0.0,
      pm10: map['pm10']?.toDouble() ?? 0.0,
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherAirQualityPayload.fromJson(String source) =>
      WeatherAirQualityPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherAirQualityPayload(co: $co, no2: $no2, o3: $o3, so2: $so2, pm2_5: $pm2_5, pm10: $pm10)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherAirQualityPayload &&
        other.co == co &&
        other.no2 == no2 &&
        other.o3 == o3 &&
        other.so2 == so2 &&
        other.pm2_5 == pm2_5 &&
        other.pm10 == pm10;
  }

  @override
  int get hashCode {
    return co.hashCode ^
        no2.hashCode ^
        o3.hashCode ^
        so2.hashCode ^
        pm2_5.hashCode ^
        pm10.hashCode;
  }
}

/// # WeatherConditionPayload
/// payload for condition data from api
class WeatherConditionPayload {
  String text;
  String icon;
  int code;

  WeatherConditionPayload({
    required this.text,
    required this.icon,
    required this.code,
  });

  WeatherConditionPayload copyWith({
    String? text,
    String? icon,
    int? code,
  }) {
    return WeatherConditionPayload(
      text: text ?? this.text,
      icon: icon ?? this.icon,
      code: code ?? this.code,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'icon': icon,
      'code': code,
    };
  }

  factory WeatherConditionPayload.fromMap(Map<String, dynamic> map) {
    return WeatherConditionPayload(
      text: map['text'] ?? '',
      icon: map['icon'] ?? '',
      code: map['code']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherConditionPayload.fromJson(String source) =>
      WeatherConditionPayload.fromMap(json.decode(source));

  @override
  String toString() =>
      'WeatherConditionPayload(text: $text, icon: $icon, code: $code)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherConditionPayload &&
        other.text == text &&
        other.icon == icon &&
        other.code == code;
  }

  @override
  int get hashCode => text.hashCode ^ icon.hashCode ^ code.hashCode;
}

/// # WeatherAlertDetailPayload
/// Payload detail for an alert
class WeatherAlertDetailPayload {
  String headline;
  String msgtype;
  String severity;
  String urgency;
  String areas;
  String category;
  String certainty;
  String event;
  String note;
  String effective;
  String expires;
  String desc;
  String instruction;

  WeatherAlertDetailPayload({
    required this.headline,
    required this.msgtype,
    required this.severity,
    required this.urgency,
    required this.areas,
    required this.category,
    required this.certainty,
    required this.event,
    required this.note,
    required this.effective,
    required this.expires,
    required this.desc,
    required this.instruction,
  });

  WeatherAlertDetailPayload copyWith({
    String? headline,
    String? msgtype,
    String? severity,
    String? urgency,
    String? areas,
    String? category,
    String? certainty,
    String? event,
    String? note,
    String? effective,
    String? expires,
    String? desc,
    String? instruction,
  }) {
    return WeatherAlertDetailPayload(
      headline: headline ?? this.headline,
      msgtype: msgtype ?? this.msgtype,
      severity: severity ?? this.severity,
      urgency: urgency ?? this.urgency,
      areas: areas ?? this.areas,
      category: category ?? this.category,
      certainty: certainty ?? this.certainty,
      event: event ?? this.event,
      note: note ?? this.note,
      effective: effective ?? this.effective,
      expires: expires ?? this.expires,
      desc: desc ?? this.desc,
      instruction: instruction ?? this.instruction,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'headline': headline,
      'msgtype': msgtype,
      'severity': severity,
      'urgency': urgency,
      'areas': areas,
      'category': category,
      'certainty': certainty,
      'event': event,
      'note': note,
      'effective': effective,
      'expires': expires,
      'desc': desc,
      'instruction': instruction,
    };
  }

  factory WeatherAlertDetailPayload.fromMap(Map<String, dynamic> map) {
    return WeatherAlertDetailPayload(
      headline: map['headline'] ?? '',
      msgtype: map['msgtype'] ?? '',
      severity: map['severity'] ?? '',
      urgency: map['urgency'] ?? '',
      areas: map['areas'] ?? '',
      category: map['category'] ?? '',
      certainty: map['certainty'] ?? '',
      event: map['event'] ?? '',
      note: map['note'] ?? '',
      effective: map['effective'] ?? '',
      expires: map['expires'] ?? '',
      desc: map['desc'] ?? '',
      instruction: map['instruction'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherAlertDetailPayload.fromJson(String source) =>
      WeatherAlertDetailPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherAlertDetailPayload(headline: $headline, msgtype: $msgtype, severity: $severity, urgency: $urgency, areas: $areas, category: $category, certainty: $certainty, event: $event, note: $note, effective: $effective, expires: $expires, desc: $desc, instruction: $instruction)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherAlertDetailPayload &&
        other.headline == headline &&
        other.msgtype == msgtype &&
        other.severity == severity &&
        other.urgency == urgency &&
        other.areas == areas &&
        other.category == category &&
        other.certainty == certainty &&
        other.event == event &&
        other.note == note &&
        other.effective == effective &&
        other.expires == expires &&
        other.desc == desc &&
        other.instruction == instruction;
  }

  @override
  int get hashCode {
    return headline.hashCode ^
        msgtype.hashCode ^
        severity.hashCode ^
        urgency.hashCode ^
        areas.hashCode ^
        category.hashCode ^
        certainty.hashCode ^
        event.hashCode ^
        note.hashCode ^
        effective.hashCode ^
        expires.hashCode ^
        desc.hashCode ^
        instruction.hashCode;
  }
}

/// # WeatherAstroDetailPayload
/// detail astronomy payload from api
class WeatherAstroDetailPayload {
  String sunrise;
  String sunset;
  String moonrise;
  String moonset;
  String moon_phase;

  WeatherAstroDetailPayload({
    required this.sunrise,
    required this.sunset,
    required this.moonrise,
    required this.moonset,
    required this.moon_phase,
  });

  WeatherAstroDetailPayload copyWith({
    String? sunrise,
    String? sunset,
    String? moonrise,
    String? moonset,
    String? moon_phase,
  }) {
    return WeatherAstroDetailPayload(
      sunrise: sunrise ?? this.sunrise,
      sunset: sunset ?? this.sunset,
      moonrise: moonrise ?? this.moonrise,
      moonset: moonset ?? this.moonset,
      moon_phase: moon_phase ?? this.moon_phase,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'sunrise': sunrise,
      'sunset': sunset,
      'moonrise': moonrise,
      'moonset': moonset,
      'moon_phase': moon_phase,
    };
  }

  factory WeatherAstroDetailPayload.fromMap(Map<String, dynamic> map) {
    return WeatherAstroDetailPayload(
      sunrise: map['sunrise'] ?? '',
      sunset: map['sunset'] ?? '',
      moonrise: map['moonrise'] ?? '',
      moonset: map['moonset'] ?? '',
      moon_phase: map['moon_phase'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherAstroDetailPayload.fromJson(String source) =>
      WeatherAstroDetailPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherAstroDetailPayload(sunrise: $sunrise, sunset: $sunset, moonrise: $moonrise, moonset: $moonset, moon_phase: $moon_phase)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherAstroDetailPayload &&
        other.sunrise == sunrise &&
        other.sunset == sunset &&
        other.moonrise == moonrise &&
        other.moonset == moonset &&
        other.moon_phase == moon_phase;
  }

  @override
  int get hashCode {
    return sunrise.hashCode ^
        sunset.hashCode ^
        moonrise.hashCode ^
        moonset.hashCode ^
        moon_phase.hashCode;
  }
}

/// # WeatherForecastDayItemPayload
/// payload for forecast in specify day
class WeatherForecastDayItemPayload {
  String date;
  int date_epoch;
  WeatherDayDetailPayload day;
  WeatherAstroDetailPayload astro;
  List<WeatherHourForecastItemPayload> hour;
  WeatherForecastDayItemPayload({
    required this.date,
    required this.date_epoch,
    required this.day,
    required this.astro,
    required this.hour,
  });

  WeatherForecastDayItemPayload copyWith({
    String? date,
    int? date_epoch,
    WeatherDayDetailPayload? day,
    WeatherAstroDetailPayload? astro,
    List<WeatherHourForecastItemPayload>? hour,
  }) {
    return WeatherForecastDayItemPayload(
      date: date ?? this.date,
      date_epoch: date_epoch ?? this.date_epoch,
      day: day ?? this.day,
      astro: astro ?? this.astro,
      hour: hour ?? this.hour,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'date': date,
      'date_epoch': date_epoch,
      'day': day.toMap(),
      'astro': astro.toMap(),
      'hour': hour.map((x) => x.toMap()).toList(),
    };
  }

  factory WeatherForecastDayItemPayload.fromMap(Map<String, dynamic> map) {
    return WeatherForecastDayItemPayload(
      date: map['date'] ?? '',
      date_epoch: map['date_epoch']?.toInt() ?? 0,
      day: WeatherDayDetailPayload.fromMap(map['day']),
      astro: WeatherAstroDetailPayload.fromMap(map['astro']),
      hour: List<WeatherHourForecastItemPayload>.from(
          map['hour']?.map((x) => WeatherHourForecastItemPayload.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherForecastDayItemPayload.fromJson(String source) =>
      WeatherForecastDayItemPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherForecastDayItemPayload(date: $date, date_epoch: $date_epoch, day: $day, astro: $astro, hour: $hour)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherForecastDayItemPayload &&
        other.date == date &&
        other.date_epoch == date_epoch &&
        other.day == day &&
        other.astro == astro &&
        listEquals(other.hour, hour);
  }

  @override
  int get hashCode {
    return date.hashCode ^
        date_epoch.hashCode ^
        day.hashCode ^
        astro.hashCode ^
        hour.hashCode;
  }
}

/// # WeatherHouseForecastItemPayload
/// pyload for hour forecast item
class WeatherHourForecastItemPayload {
  int time_epoch;
  String time;
  double temp_c;
  double temp_f;
  int is_day;
  WeatherConditionPayload condition;
  double wind_mph;
  double wind_kph;
  int wind_degree;
  String wind_dir;
  double pressure_mb;
  double pressure_in;
  double precip_mm;
  double precip_in;
  int humidity;
  int cloud;
  double feelslike_c;
  double feelslike_f;
  double windchill_c;
  double windchill_f;
  double heatindex_c;
  double heatindex_f;
  double dewpoint_c;
  double dewpoint_f;
  int will_it_rain;
  int chance_of_rain;
  int will_it_snow;
  int chance_of_snow;
  double vis_km;
  double vis_miles;
  double gust_mph;
  double gust_kph;
  double uv;

  WeatherHourForecastItemPayload({
    required this.time_epoch,
    required this.time,
    required this.temp_c,
    required this.temp_f,
    required this.is_day,
    required this.condition,
    required this.wind_mph,
    required this.wind_kph,
    required this.wind_degree,
    required this.wind_dir,
    required this.pressure_mb,
    required this.pressure_in,
    required this.precip_mm,
    required this.precip_in,
    required this.humidity,
    required this.cloud,
    required this.feelslike_c,
    required this.feelslike_f,
    required this.windchill_c,
    required this.windchill_f,
    required this.heatindex_c,
    required this.heatindex_f,
    required this.dewpoint_c,
    required this.dewpoint_f,
    required this.will_it_rain,
    required this.chance_of_rain,
    required this.will_it_snow,
    required this.chance_of_snow,
    required this.vis_km,
    required this.vis_miles,
    required this.gust_mph,
    required this.gust_kph,
    required this.uv,
  });

  WeatherHourForecastItemPayload copyWith({
    int? time_epoch,
    String? time,
    double? temp_c,
    double? temp_f,
    int? is_day,
    WeatherConditionPayload? condition,
    double? wind_mph,
    double? wind_kph,
    int? wind_degree,
    String? wind_dir,
    double? pressure_mb,
    double? pressure_in,
    double? precip_mm,
    double? precip_in,
    int? humidity,
    int? cloud,
    double? feelslike_c,
    double? feelslike_f,
    double? windchill_c,
    double? windchill_f,
    double? heatindex_c,
    double? heatindex_f,
    double? dewpoint_c,
    double? dewpoint_f,
    int? will_it_rain,
    int? chance_of_rain,
    int? will_it_snow,
    int? chance_of_snow,
    double? vis_km,
    double? vis_miles,
    double? gust_mph,
    double? gust_kph,
    double? uv,
  }) {
    return WeatherHourForecastItemPayload(
      time_epoch: time_epoch ?? this.time_epoch,
      time: time ?? this.time,
      temp_c: temp_c ?? this.temp_c,
      temp_f: temp_f ?? this.temp_f,
      is_day: is_day ?? this.is_day,
      condition: condition ?? this.condition,
      wind_mph: wind_mph ?? this.wind_mph,
      wind_kph: wind_kph ?? this.wind_kph,
      wind_degree: wind_degree ?? this.wind_degree,
      wind_dir: wind_dir ?? this.wind_dir,
      pressure_mb: pressure_mb ?? this.pressure_mb,
      pressure_in: pressure_in ?? this.pressure_in,
      precip_mm: precip_mm ?? this.precip_mm,
      precip_in: precip_in ?? this.precip_in,
      humidity: humidity ?? this.humidity,
      cloud: cloud ?? this.cloud,
      feelslike_c: feelslike_c ?? this.feelslike_c,
      feelslike_f: feelslike_f ?? this.feelslike_f,
      windchill_c: windchill_c ?? this.windchill_c,
      windchill_f: windchill_f ?? this.windchill_f,
      heatindex_c: heatindex_c ?? this.heatindex_c,
      heatindex_f: heatindex_f ?? this.heatindex_f,
      dewpoint_c: dewpoint_c ?? this.dewpoint_c,
      dewpoint_f: dewpoint_f ?? this.dewpoint_f,
      will_it_rain: will_it_rain ?? this.will_it_rain,
      chance_of_rain: chance_of_rain ?? this.chance_of_rain,
      will_it_snow: will_it_snow ?? this.will_it_snow,
      chance_of_snow: chance_of_snow ?? this.chance_of_snow,
      vis_km: vis_km ?? this.vis_km,
      vis_miles: vis_miles ?? this.vis_miles,
      gust_mph: gust_mph ?? this.gust_mph,
      gust_kph: gust_kph ?? this.gust_kph,
      uv: uv ?? this.uv,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'time_epoch': time_epoch,
      'time': time,
      'temp_c': temp_c,
      'temp_f': temp_f,
      'is_day': is_day,
      'condition': condition.toMap(),
      'wind_mph': wind_mph,
      'wind_kph': wind_kph,
      'wind_degree': wind_degree,
      'wind_dir': wind_dir,
      'pressure_mb': pressure_mb,
      'pressure_in': pressure_in,
      'precip_mm': precip_mm,
      'precip_in': precip_in,
      'humidity': humidity,
      'cloud': cloud,
      'feelslike_c': feelslike_c,
      'feelslike_f': feelslike_f,
      'windchill_c': windchill_c,
      'windchill_f': windchill_f,
      'heatindex_c': heatindex_c,
      'heatindex_f': heatindex_f,
      'dewpoint_c': dewpoint_c,
      'dewpoint_f': dewpoint_f,
      'will_it_rain': will_it_rain,
      'chance_of_rain': chance_of_rain,
      'will_it_snow': will_it_snow,
      'chance_of_snow': chance_of_snow,
      'vis_km': vis_km,
      'vis_miles': vis_miles,
      'gust_mph': gust_mph,
      'gust_kph': gust_kph,
      'uv': uv,
    };
  }

  factory WeatherHourForecastItemPayload.fromMap(Map<String, dynamic> map) {
    return WeatherHourForecastItemPayload(
      time_epoch: map['time_epoch']?.toInt() ?? 0,
      time: map['time'] ?? '',
      temp_c: map['temp_c']?.toDouble() ?? 0.0,
      temp_f: map['temp_f']?.toDouble() ?? 0.0,
      is_day: map['is_day']?.toInt() ?? 0,
      condition: WeatherConditionPayload.fromMap(map['condition']),
      wind_mph: map['wind_mph']?.toDouble() ?? 0.0,
      wind_kph: map['wind_kph']?.toDouble() ?? 0.0,
      wind_degree: map['wind_degree']?.toInt() ?? 0,
      wind_dir: map['wind_dir'] ?? '',
      pressure_mb: map['pressure_mb']?.toDouble() ?? 0.0,
      pressure_in: map['pressure_in']?.toDouble() ?? 0.0,
      precip_mm: map['precip_mm']?.toDouble() ?? 0.0,
      precip_in: map['precip_in']?.toDouble() ?? 0.0,
      humidity: map['humidity']?.toInt() ?? 0,
      cloud: map['cloud']?.toInt() ?? 0,
      feelslike_c: map['feelslike_c']?.toDouble() ?? 0.0,
      feelslike_f: map['feelslike_f']?.toDouble() ?? 0.0,
      windchill_c: map['windchill_c']?.toDouble() ?? 0.0,
      windchill_f: map['windchill_f']?.toDouble() ?? 0.0,
      heatindex_c: map['heatindex_c']?.toDouble() ?? 0.0,
      heatindex_f: map['heatindex_f']?.toDouble() ?? 0.0,
      dewpoint_c: map['dewpoint_c']?.toDouble() ?? 0.0,
      dewpoint_f: map['dewpoint_f']?.toDouble() ?? 0.0,
      will_it_rain: map['will_it_rain']?.toInt() ?? 0,
      chance_of_rain: map['chance_of_rain']?.toInt() ?? 0,
      will_it_snow: map['will_it_snow']?.toInt() ?? 0,
      chance_of_snow: map['chance_of_snow']?.toInt() ?? 0,
      vis_km: map['vis_km']?.toDouble() ?? 0.0,
      vis_miles: map['vis_miles']?.toDouble() ?? 0.0,
      gust_mph: map['gust_mph']?.toDouble() ?? 0.0,
      gust_kph: map['gust_kph']?.toDouble() ?? 0.0,
      uv: map['uv']?.toDouble() ?? 0.0,
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherHourForecastItemPayload.fromJson(String source) =>
      WeatherHourForecastItemPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherHourForecastItemPayload(time_epoch: $time_epoch, time: $time, temp_c: $temp_c, temp_f: $temp_f, is_day: $is_day, condition: $condition, wind_mph: $wind_mph, wind_kph: $wind_kph, wind_degree: $wind_degree, wind_dir: $wind_dir, pressure_mb: $pressure_mb, pressure_in: $pressure_in, precip_mm: $precip_mm, precip_in: $precip_in, humidity: $humidity, cloud: $cloud, feelslike_c: $feelslike_c, feelslike_f: $feelslike_f, windchill_c: $windchill_c, windchill_f: $windchill_f, heatindex_c: $heatindex_c, heatindex_f: $heatindex_f, dewpoint_c: $dewpoint_c, dewpoint_f: $dewpoint_f, will_it_rain: $will_it_rain, chance_of_rain: $chance_of_rain, will_it_snow: $will_it_snow, chance_of_snow: $chance_of_snow, vis_km: $vis_km, vis_miles: $vis_miles, gust_mph: $gust_mph, gust_kph: $gust_kph, uv: $uv)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherHourForecastItemPayload &&
        other.time_epoch == time_epoch &&
        other.time == time &&
        other.temp_c == temp_c &&
        other.temp_f == temp_f &&
        other.is_day == is_day &&
        other.condition == condition &&
        other.wind_mph == wind_mph &&
        other.wind_kph == wind_kph &&
        other.wind_degree == wind_degree &&
        other.wind_dir == wind_dir &&
        other.pressure_mb == pressure_mb &&
        other.pressure_in == pressure_in &&
        other.precip_mm == precip_mm &&
        other.precip_in == precip_in &&
        other.humidity == humidity &&
        other.cloud == cloud &&
        other.feelslike_c == feelslike_c &&
        other.feelslike_f == feelslike_f &&
        other.windchill_c == windchill_c &&
        other.windchill_f == windchill_f &&
        other.heatindex_c == heatindex_c &&
        other.heatindex_f == heatindex_f &&
        other.dewpoint_c == dewpoint_c &&
        other.dewpoint_f == dewpoint_f &&
        other.will_it_rain == will_it_rain &&
        other.chance_of_rain == chance_of_rain &&
        other.will_it_snow == will_it_snow &&
        other.chance_of_snow == chance_of_snow &&
        other.vis_km == vis_km &&
        other.vis_miles == vis_miles &&
        other.gust_mph == gust_mph &&
        other.gust_kph == gust_kph &&
        other.uv == uv;
  }

  @override
  int get hashCode {
    return time_epoch.hashCode ^
        time.hashCode ^
        temp_c.hashCode ^
        temp_f.hashCode ^
        is_day.hashCode ^
        condition.hashCode ^
        wind_mph.hashCode ^
        wind_kph.hashCode ^
        wind_degree.hashCode ^
        wind_dir.hashCode ^
        pressure_mb.hashCode ^
        pressure_in.hashCode ^
        precip_mm.hashCode ^
        precip_in.hashCode ^
        humidity.hashCode ^
        cloud.hashCode ^
        feelslike_c.hashCode ^
        feelslike_f.hashCode ^
        windchill_c.hashCode ^
        windchill_f.hashCode ^
        heatindex_c.hashCode ^
        heatindex_f.hashCode ^
        dewpoint_c.hashCode ^
        dewpoint_f.hashCode ^
        will_it_rain.hashCode ^
        chance_of_rain.hashCode ^
        will_it_snow.hashCode ^
        chance_of_snow.hashCode ^
        vis_km.hashCode ^
        vis_miles.hashCode ^
        gust_mph.hashCode ^
        gust_kph.hashCode ^
        uv.hashCode;
  }
}

/// # WeatherDayDetailPayload
/// detail payload for day weather
class WeatherDayDetailPayload {
  double maxtemp_c;
  double maxtemp_f;
  double mintemp_c;
  double mintemp_f;
  double avgtemp_c;
  double avgtemp_f;
  double maxwind_mph;
  double maxwind_kph;
  double totalprecip_mm;
  double totalprecip_in;
  double avgvis_km;
  double avgvis_miles;
  int avghumidity;
  WeatherConditionPayload condition;
  double uv;

  WeatherDayDetailPayload({
    required this.maxtemp_c,
    required this.maxtemp_f,
    required this.mintemp_c,
    required this.mintemp_f,
    required this.avgtemp_c,
    required this.avgtemp_f,
    required this.maxwind_mph,
    required this.maxwind_kph,
    required this.totalprecip_mm,
    required this.totalprecip_in,
    required this.avgvis_km,
    required this.avgvis_miles,
    required this.avghumidity,
    required this.condition,
    required this.uv,
  });

  WeatherDayDetailPayload copyWith({
    double? maxtemp_c,
    double? maxtemp_f,
    double? mintemp_c,
    double? mintemp_f,
    double? avgtemp_c,
    double? avgtemp_f,
    double? maxwind_mph,
    double? maxwind_kph,
    double? totalprecip_mm,
    double? totalprecip_in,
    double? avgvis_km,
    double? avgvis_miles,
    int? avghumidity,
    WeatherConditionPayload? condition,
    double? uv,
  }) {
    return WeatherDayDetailPayload(
      maxtemp_c: maxtemp_c ?? this.maxtemp_c,
      maxtemp_f: maxtemp_f ?? this.maxtemp_f,
      mintemp_c: mintemp_c ?? this.mintemp_c,
      mintemp_f: mintemp_f ?? this.mintemp_f,
      avgtemp_c: avgtemp_c ?? this.avgtemp_c,
      avgtemp_f: avgtemp_f ?? this.avgtemp_f,
      maxwind_mph: maxwind_mph ?? this.maxwind_mph,
      maxwind_kph: maxwind_kph ?? this.maxwind_kph,
      totalprecip_mm: totalprecip_mm ?? this.totalprecip_mm,
      totalprecip_in: totalprecip_in ?? this.totalprecip_in,
      avgvis_km: avgvis_km ?? this.avgvis_km,
      avgvis_miles: avgvis_miles ?? this.avgvis_miles,
      avghumidity: avghumidity ?? this.avghumidity,
      condition: condition ?? this.condition,
      uv: uv ?? this.uv,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'maxtemp_c': maxtemp_c,
      'maxtemp_f': maxtemp_f,
      'mintemp_c': mintemp_c,
      'mintemp_f': mintemp_f,
      'avgtemp_c': avgtemp_c,
      'avgtemp_f': avgtemp_f,
      'maxwind_mph': maxwind_mph,
      'maxwind_kph': maxwind_kph,
      'totalprecip_mm': totalprecip_mm,
      'totalprecip_in': totalprecip_in,
      'avgvis_km': avgvis_km,
      'avgvis_miles': avgvis_miles,
      'avghumidity': avghumidity,
      'condition': condition.toMap(),
      'uv': uv,
    };
  }

  factory WeatherDayDetailPayload.fromMap(Map<String, dynamic> map) {
    return WeatherDayDetailPayload(
      maxtemp_c: map['maxtemp_c']?.toDouble() ?? 0.0,
      maxtemp_f: map['maxtemp_f']?.toDouble() ?? 0.0,
      mintemp_c: map['mintemp_c']?.toDouble() ?? 0.0,
      mintemp_f: map['mintemp_f']?.toDouble() ?? 0.0,
      avgtemp_c: map['avgtemp_c']?.toDouble() ?? 0.0,
      avgtemp_f: map['avgtemp_f']?.toDouble() ?? 0.0,
      maxwind_mph: map['maxwind_mph']?.toDouble() ?? 0.0,
      maxwind_kph: map['maxwind_kph']?.toDouble() ?? 0.0,
      totalprecip_mm: map['totalprecip_mm']?.toDouble() ?? 0.0,
      totalprecip_in: map['totalprecip_in']?.toDouble() ?? 0.0,
      avgvis_km: map['avgvis_km']?.toDouble() ?? 0.0,
      avgvis_miles: map['avgvis_miles']?.toDouble() ?? 0.0,
      avghumidity: map['avghumidity']?.toInt() ?? 0,
      condition: WeatherConditionPayload.fromMap(map['condition']),
      uv: map['uv']?.toDouble() ?? 0.0,
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherDayDetailPayload.fromJson(String source) =>
      WeatherDayDetailPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherDayDetailPayload(maxtemp_c: $maxtemp_c, maxtemp_f: $maxtemp_f, mintemp_c: $mintemp_c, mintemp_f: $mintemp_f, avgtemp_c: $avgtemp_c, avgtemp_f: $avgtemp_f, maxwind_mph: $maxwind_mph, maxwind_kph: $maxwind_kph, totalprecip_mm: $totalprecip_mm, totalprecip_in: $totalprecip_in, avgvis_km: $avgvis_km, avgvis_miles: $avgvis_miles, avghumidity: $avghumidity, condition: $condition, uv: $uv)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherDayDetailPayload &&
        other.maxtemp_c == maxtemp_c &&
        other.maxtemp_f == maxtemp_f &&
        other.mintemp_c == mintemp_c &&
        other.mintemp_f == mintemp_f &&
        other.avgtemp_c == avgtemp_c &&
        other.avgtemp_f == avgtemp_f &&
        other.maxwind_mph == maxwind_mph &&
        other.maxwind_kph == maxwind_kph &&
        other.totalprecip_mm == totalprecip_mm &&
        other.totalprecip_in == totalprecip_in &&
        other.avgvis_km == avgvis_km &&
        other.avgvis_miles == avgvis_miles &&
        other.avghumidity == avghumidity &&
        other.condition == condition &&
        other.uv == uv;
  }

  @override
  int get hashCode {
    return maxtemp_c.hashCode ^
        maxtemp_f.hashCode ^
        mintemp_c.hashCode ^
        mintemp_f.hashCode ^
        avgtemp_c.hashCode ^
        avgtemp_f.hashCode ^
        maxwind_mph.hashCode ^
        maxwind_kph.hashCode ^
        totalprecip_mm.hashCode ^
        totalprecip_in.hashCode ^
        avgvis_km.hashCode ^
        avgvis_miles.hashCode ^
        avghumidity.hashCode ^
        condition.hashCode ^
        uv.hashCode;
  }
}

/// # WeatherSportItemCard
/// sport detail based on weather
class WeatherSportItemPayload {
  String stadium;
  String country;
  String region;
  String tournament;
  String start;
  String match;

  WeatherSportItemPayload({
    required this.stadium,
    required this.country,
    required this.region,
    required this.tournament,
    required this.start,
    required this.match,
  });

  WeatherSportItemPayload copyWith({
    String? stadium,
    String? country,
    String? region,
    String? tournament,
    String? start,
    String? match,
  }) {
    return WeatherSportItemPayload(
      stadium: stadium ?? this.stadium,
      country: country ?? this.country,
      region: region ?? this.region,
      tournament: tournament ?? this.tournament,
      start: start ?? this.start,
      match: match ?? this.match,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'stadium': stadium,
      'country': country,
      'region': region,
      'tournament': tournament,
      'start': start,
      'match': match,
    };
  }

  factory WeatherSportItemPayload.fromMap(Map<String, dynamic> map) {
    return WeatherSportItemPayload(
      stadium: map['stadium'] ?? '',
      country: map['country'] ?? '',
      region: map['region'] ?? '',
      tournament: map['tournament'] ?? '',
      start: map['start'] ?? '',
      match: map['match'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherSportItemPayload.fromJson(String source) =>
      WeatherSportItemPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WeatherSportItemPayload(stadium: $stadium, country: $country, region: $region, tournament: $tournament, start: $start, match: $match)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherSportItemPayload &&
        other.stadium == stadium &&
        other.country == country &&
        other.region == region &&
        other.tournament == tournament &&
        other.start == start &&
        other.match == match;
  }

  @override
  int get hashCode {
    return stadium.hashCode ^
        country.hashCode ^
        region.hashCode ^
        tournament.hashCode ^
        start.hashCode ^
        match.hashCode;
  }
}

/// # WeatherForecastPayload
/// forecast item payload
class WeatherForecastPayload {
  List<WeatherForecastDayItemPayload> forecastday;

  WeatherForecastPayload({
    required this.forecastday,
  });

  WeatherForecastPayload copyWith({
    List<WeatherForecastDayItemPayload>? forecastday,
  }) {
    return WeatherForecastPayload(
      forecastday: forecastday ?? this.forecastday,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'forecastday': forecastday.map((x) => x.toMap()).toList(),
    };
  }

  factory WeatherForecastPayload.fromMap(Map<String, dynamic> map) {
    return WeatherForecastPayload(
      forecastday: List<WeatherForecastDayItemPayload>.from(map['forecastday']
          ?.map((x) => WeatherForecastDayItemPayload.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherForecastPayload.fromJson(String source) =>
      WeatherForecastPayload.fromMap(json.decode(source));

  @override
  String toString() => 'WeatherForecastPayload(forecastday: $forecastday)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherForecastPayload &&
        listEquals(other.forecastday, forecastday);
  }

  @override
  int get hashCode => forecastday.hashCode;
}

/// # WeatherAlertsPayload
/// payload for listing alerts
class WeatherAlertsPayload {
  List<WeatherAlertDetailPayload> alert;

  WeatherAlertsPayload({
    required this.alert,
  });

  WeatherAlertsPayload copyWith({
    List<WeatherAlertDetailPayload>? alert,
  }) {
    return WeatherAlertsPayload(
      alert: alert ?? this.alert,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'alert': alert.map((x) => x.toMap()).toList(),
    };
  }

  factory WeatherAlertsPayload.fromMap(Map<String, dynamic> map) {
    return WeatherAlertsPayload(
      alert: List<WeatherAlertDetailPayload>.from(
          map['alert']?.map((x) => WeatherAlertDetailPayload.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherAlertsPayload.fromJson(String source) =>
      WeatherAlertsPayload.fromMap(json.decode(source));

  @override
  String toString() => 'WeatherAlertsPayload(alert: $alert)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherAlertsPayload && listEquals(other.alert, alert);
  }

  @override
  int get hashCode => alert.hashCode;
}

/// # GeneralForecastPyload
/// payload for general forecast
class GeneralForecastPayload {
  WeatherLocationPayload location;
  CurrentDetailPayload current;
  WeatherForecastPayload forecast;
  WeatherAlertsPayload alerts;

  GeneralForecastPayload({
    required this.location,
    required this.current,
    required this.forecast,
    required this.alerts,
  });

  GeneralForecastPayload copyWith({
    WeatherLocationPayload? location,
    CurrentDetailPayload? current,
    WeatherForecastPayload? forecast,
    WeatherAlertsPayload? alerts,
  }) {
    return GeneralForecastPayload(
      location: location ?? this.location,
      current: current ?? this.current,
      forecast: forecast ?? this.forecast,
      alerts: alerts ?? this.alerts,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'location': location.toMap(),
      'current': current.toMap(),
      'forecast': forecast.toMap(),
      'alerts': alerts.toMap(),
    };
  }

  factory GeneralForecastPayload.fromMap(Map<String, dynamic> map) {
    return GeneralForecastPayload(
      location: WeatherLocationPayload.fromMap(map['location']),
      current: CurrentDetailPayload.fromMap(map['current']),
      forecast: WeatherForecastPayload.fromMap(map['forecast']),
      alerts: WeatherAlertsPayload.fromMap(map['alerts']),
    );
  }

  String toJson() => json.encode(toMap());

  factory GeneralForecastPayload.fromJson(String source) =>
      GeneralForecastPayload.fromMap(json.decode(source));

  @override
  String toString() {
    return 'GeneralForecastPayload(location: $location, current: $current, forecast: $forecast, alerts: $alerts)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GeneralForecastPayload &&
        other.location == location &&
        other.current == current &&
        other.forecast == forecast &&
        other.alerts == alerts;
  }

  @override
  int get hashCode {
    return location.hashCode ^
        current.hashCode ^
        forecast.hashCode ^
        alerts.hashCode;
  }
}

/// #WeatherGeneralSportsPayload
/// general payload for sports
class WeatherGeneralSportsPayload {
  List<WeatherSportItemPayload> football;
  List<WeatherSportItemPayload> cricket;
  List<WeatherSportItemPayload> golf;

  WeatherGeneralSportsPayload({
    required this.football,
    required this.cricket,
    required this.golf,
  });

  WeatherGeneralSportsPayload copyWith({
    List<WeatherSportItemPayload>? football,
    List<WeatherSportItemPayload>? cricket,
    List<WeatherSportItemPayload>? golf,
  }) {
    return WeatherGeneralSportsPayload(
      football: football ?? this.football,
      cricket: cricket ?? this.cricket,
      golf: golf ?? this.golf,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'football': football.map((x) => x.toMap()).toList(),
      'cricket': cricket.map((x) => x.toMap()).toList(),
      'golf': golf.map((x) => x.toMap()).toList(),
    };
  }

  factory WeatherGeneralSportsPayload.fromMap(Map<String, dynamic> map) {
    return WeatherGeneralSportsPayload(
      football: List<WeatherSportItemPayload>.from(
          map['football']?.map((x) => WeatherSportItemPayload.fromMap(x))),
      cricket: List<WeatherSportItemPayload>.from(
          map['cricket']?.map((x) => WeatherSportItemPayload.fromMap(x))),
      golf: List<WeatherSportItemPayload>.from(
          map['golf']?.map((x) => WeatherSportItemPayload.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherGeneralSportsPayload.fromJson(String source) =>
      WeatherGeneralSportsPayload.fromMap(json.decode(source));

  @override
  String toString() =>
      'WeatherGeneralSportsPayload(football: $football, cricket: $cricket, golf: $golf)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WeatherGeneralSportsPayload &&
        listEquals(other.football, football) &&
        listEquals(other.cricket, cricket) &&
        listEquals(other.golf, golf);
  }

  @override
  int get hashCode => football.hashCode ^ cricket.hashCode ^ golf.hashCode;
}
