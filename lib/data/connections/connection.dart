import 'package:dio/dio.dart';
import 'package:meteo/data/connections/endpoint.dart';

/// # MeteoConnection
/// connection service for api consume
/// base on dio, and override some settings,
///
class MeteoConnection {
  // the dio configuration
  // api consumer

  // basic configuration for api connection

  final _dio = Dio(
    BaseOptions(
      baseUrl: MeteoEndpoints.baseUrlWeather,
      connectTimeout: 5000,
      receiveTimeout: 8000,
      sendTimeout: 6000,
      queryParameters: {
        "key": MeteoEndpoints.apiKey,
      },
    ),
  );

  // the connection
  // specify for api
  Dio get apiConnection => _dio;
}
