/// # MeteoEndpoints
/// all of the api endpoint come here
class MeteoEndpoints {
  // this property is secret, make sure you clean it up
  static const String apiKey = "3d1792ce21854ac79df65903220401";

  // the base url comes
  static const String baseUrlWeather = "http://api.weatherapi.com/v1";
  static const String forecast = "/forecast.json";
  static const String locationAutocomplete = "/search.json";
  static const String sports = "/sports.json";
}
