import 'package:hive_flutter/hive_flutter.dart';

/// # AccountRepository
/// repository for access data both local and cloud for acount
/// this can be some initial data, or other stuf

class AccountRepository {
  /// ## isOnboarded
  /// check the onboarding of user
  /// is user already on boarding or not
  /// will return boolean type
  ///
  Future<bool> isOnboarded() async {
    var _box = await Hive.openBox("account");

    try {
      // we need to check the current data in local data
      return _box.get("onboarded", defaultValue: false) as bool;
    } catch (e) {
      // when something error found
      // we will return false as default
      return false;
    }
  }

  /// # userOnboarded
  /// save the local properties and
  /// indicate if the user is already onboarded
  ///
  Future<void> userOnboarded() async {
    var _box = await Hive.openBox("account");

    try {
      // will try to save into local storage
      _box.put("onboarded", true);
    } catch (e) {
      // well if something error found,
      // let's try again
      _box.put("onboarded", true);
    }
  }
}
