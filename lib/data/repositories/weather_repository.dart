import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meteo/data/connections/connection.dart';
import 'package:meteo/data/connections/endpoint.dart';
import 'package:meteo/data/payloads/weather_payload.dart';

/// # WeatherRepository
class WeatherRepository extends MeteoConnection {
  /// ## Load Forecast
  ///
  /// load the detail of forecast
  /// there some parameters need to be adding
  ///
  /// ### 1. `queryLocation` this can be location name, postal code,
  /// map lat, long
  ///
  /// ### 2. `showAlerts` you can show or hide the alerts
  ///
  /// ### 3. `showAirQuality` you can show or hide the air quality
  ///
  /// ### 4. `days` the days count, and for now max is 7
  ///
  /// ### 5. `locale` the locale of api, you can also set the locale
  /// and the language will be changed
  ///
  ///
  Future<GeneralForecastPayload?> loadForecast({
    required String queryLocation,
    bool showAlerts = true,
    bool showAirQuality = true,
    int days = 7,
    Locale locale = const Locale("en"),
  }) async {
    // define params
    var params = {
      "q": queryLocation,
      "days": days,
      "alerts": showAlerts ? "yes" : "no",
      "aqi": showAirQuality ? "yes" : "no",
      "lang": locale.languageCode,
    };

    try {
      var res = await apiConnection.get(MeteoEndpoints.forecast,
          queryParameters: params);

      return GeneralForecastPayload.fromMap(res.data);
    } on DioError {
      return null;
    }
  }

  /// # loadLocationsAutoComplete
  /// search the location for auto complete
  ///
  /// there some parameters you need to add
  ///
  /// 1. `queryLocation` city, location, state name
  /// 2. `locale` your locale for user can easily consume the data
  Future<List<WeatherLocationPayload>> loadLocationsAutoComplete({
    required String queryLocation,
    Locale locale = const Locale("en"),
  }) async {
    // define params
    var params = {
      "q": queryLocation,
      "lang": locale.languageCode,
    };

    // load the source
    try {
      var res = await apiConnection.get(MeteoEndpoints.locationAutocomplete,
          queryParameters: params);

      // the connection success
      // we need to return the response
      return List.from(
          res.data.map((e) => WeatherLocationPayload.fromMap(e)).toList());
    } on DioError {
      // catch some error
      // return empty when not found
      return List.empty();
    }
  }

  /// # loadSports
  /// search the location for auto complete
  ///
  /// there some parameters you need to add
  ///
  /// 1. `queryLocation` city, location, state name
  /// 2. `locale` your locale for user can easily consume the data
  Future<WeatherGeneralSportsPayload?> loadSports({
    required String queryLocation,
    Locale locale = const Locale("en"),
  }) async {
    // define params
    var params = {
      "q": queryLocation,
      "lang": locale.languageCode,
    };

    // load the source
    try {
      var res = await apiConnection.get(MeteoEndpoints.sports,
          queryParameters: params);

      // the connection success
      // we need to return the response
      return WeatherGeneralSportsPayload.fromMap(res.data);
    } on DioError {
      // catch some error
      // return empty when not found
      return null;
    }
  }
}
