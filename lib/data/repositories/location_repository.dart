import 'package:location/location.dart';

class LocationRepository {
  final Location _location = Location();

  // init the location service
  // will doing init and some configuration service
  Future<void> _initLocation() async {
    // check the service was enable
    bool locationEnabled = await _location.serviceEnabled();

    // ok, when the location is not enable, we
    // need to request to enable it
    if (!locationEnabled) {
      locationEnabled = await _location.requestService();

      // ok, we need to check once agin, to ensure the location was
      // enabled
      if (!locationEnabled) {
        return;
      }
    }

    // well, we we see that the service was successfully enabled
    // we need to check the current permision
    var permision = await _location.hasPermission();

    // we need to ensure that the permision is enable
    // for this app
    if (permision == PermissionStatus.denied ||
        permision == PermissionStatus.deniedForever) {
      // hmm, the permision was denied,
      // we need to request a nre permision for this app and
      // granted it
      permision = await _location.requestPermission();

      // we need to ensure once again
      if (permision == PermissionStatus.granted ||
          permision == PermissionStatus.grantedLimited) {
        return;
      }
    }

    // ensure that the location service, also can run in background
    _location.enableBackgroundMode();
  }

  /// ## loadCurrentLocation
  /// load the current location with high accuracy
  Future<LocationData> loadCurrentLocation() async {
    // we need to init the service location
    await _initLocation();

    // after that, we will take the location data
    return await _location.getLocation();
  }
}
